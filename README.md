### About the application
- Application is pretty straightforward just adding side notes below
- Hard coded a list for the different categories. Catergories are random and from the internet.
- No real api calls were made, still connected `redux-saga` to semi implement what an api call would look like in terms of logic.
- A user can delete an expense by `long pressing` the `Expense` component


### Getting Started

- To get started setting up your local development, first clone this repo to a local directory and then run `yarn install` to install various dependencies
- Install `react-native-cli` by running the command `npm install -g react-native-cli`
- Then execute `cd ios && pod install && cd ..` to initialize pods
- Finally, execute `npm run ios` || `npm run android` to start the JS bundler and app

### Trouble Shooting
- If the app cannot be launched with `npm run ios` || `npm run android` please execute app from `Xcode` || `Android Studio`







