import {createStore, combineReducers, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import expenseReducer from './expenseReducer';
import globalReducer from './globalReducer';

import {watcherSaga} from '../saga';

const reducer = combineReducers({
  expense: expenseReducer,
  global: globalReducer,
});

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

const store = createStore(reducer, {}, applyMiddleware(...middleware));

sagaMiddleware.run(watcherSaga);

export default store;
