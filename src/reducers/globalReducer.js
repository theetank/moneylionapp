import moment from 'moment-timezone';
import {
  UPLOAD_EXPENSE_SUCCESS,
  REMOVE_EXPENSE_STARTED,
  REMOVE_EXPENSE_FAILED,
  REMOVE_EXPENSE_SUCCES,
} from '../actionTypes/expense';

const INITIAL_STATE = {
  dashboard: {
    today: 0,
    week: 0,
    month: 0,
    year: 0,
  },
  expenses: [],
  loading: false,
  user: {
    name: 'Frank Gutierrez',
  },
  error: null,
};

const globalReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REMOVE_EXPENSE_STARTED:
      return {
        ...state,
        loading: true,
      };
    case REMOVE_EXPENSE_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case REMOVE_EXPENSE_SUCCES:
      //today check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('d').valueOf() &&
        moment(action.payload.date).valueOf() <= moment().endOf('d').valueOf()
      ) {
        state.dashboard.today -= Number(action.payload.amount);
      }
      //week check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('w').valueOf() &&
        moment(action.payload.date).valueOf() <= moment().endOf('w').valueOf()
      ) {
        state.dashboard.week -= Number(action.payload.amount);
      }
      // month check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('month').valueOf() &&
        moment(action.payload.date).valueOf() <=
          moment().endOf('month').valueOf()
      ) {
        state.dashboard.month -= Number(action.payload.amount);
      }
      // year check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('y').valueOf() &&
        moment(action.payload.date).valueOf() <= moment().endOf('y').valueOf()
      ) {
        state.dashboard.year -= Number(action.payload.amount);
      }
      return {
        ...state,
        error: null,
        loading: false,
        expenses: state.expenses.filter(i => i.id !== action.payload.id),
      };
    case UPLOAD_EXPENSE_SUCCESS:
      const holder = [action.payload, ...state.expenses];
      //today check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('d').valueOf() &&
        moment(action.payload.date).valueOf() <= moment().endOf('d').valueOf()
      ) {
        state.dashboard.today += Number(action.payload.amount);
      }
      //week check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('w').valueOf() &&
        moment(action.payload.date).valueOf() <= moment().endOf('w').valueOf()
      ) {
        state.dashboard.week += Number(action.payload.amount);
      }
      // month check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('month').valueOf() &&
        moment(action.payload.date).valueOf() <=
          moment().endOf('month').valueOf()
      ) {
        state.dashboard.month += Number(action.payload.amount);
      }
      // year check
      if (
        moment(action.payload.date).valueOf() >=
          moment().startOf('y').valueOf() &&
        moment(action.payload.date).valueOf() <= moment().endOf('y').valueOf()
      ) {
        state.dashboard.year += Number(action.payload.amount);
      }
      return {
        ...state,
        expenses: holder.sort(
          (a, b) => moment(a.date).valueOf() > moment(b.date).valueOf(),
        ), //might need to spread
      };
    default:
      return state;
  }
};

export default globalReducer;
