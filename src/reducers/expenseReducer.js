import {
  UPLOAD_EXPENSE_STARTED,
  UPLOAD_EXPENSE_FAILED,
  UPLOAD_EXPENSE_SUCCESS,
  ADD_EXPENSE_TITLE,
  ADD_EXPENSE_AMOUNT,
  ADD_EXPENSE_DATE,
  ADD_EXPENSE_CATEGORY,
  RESET_EXPENSE_FORM,
  SET_FORM_ERROR,
  TOGGLE_VISIBLE,
} from '../actionTypes/expense';

const INITIAL_STATE = {
  title: null,
  amount: null,
  date: null,
  category: null,
  loading: false,
  error: null,
  isVisible: false,
};

const expenseReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TOGGLE_VISIBLE:
      return {
        ...state,
        isVisible: !state.isVisible,
      };
    case ADD_EXPENSE_TITLE:
      return {
        ...state,
        title: action.payload,
      };
    case ADD_EXPENSE_AMOUNT:
      return {
        ...state,
        amount: action.payload,
      };
    case ADD_EXPENSE_DATE:
      return {
        ...state,
        date: action.payload,
      };
    case ADD_EXPENSE_CATEGORY:
      return {
        ...state,
        category: action.payload,
      };
    case UPLOAD_EXPENSE_STARTED:
      return {
        ...state,
        loading: true,
      };
    case SET_FORM_ERROR:
    case UPLOAD_EXPENSE_FAILED:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case RESET_EXPENSE_FORM:
    case UPLOAD_EXPENSE_SUCCESS:
      return INITIAL_STATE;

    default:
      return state;
  }
};

export default expenseReducer;
