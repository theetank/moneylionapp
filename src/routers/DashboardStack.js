import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';
import {Dashboard} from '../components/screens';

const Stack = createNativeStackNavigator();

export default function DashboardStack() {
  return (
    <Stack.Navigator initialRouteName="Dashboard">
      <Stack.Screen name="Dashboard" component={Dashboard} />
    </Stack.Navigator>
  );
}
