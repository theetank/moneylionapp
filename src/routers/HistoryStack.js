import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';
import {History} from '../components/screens';

const Stack = createNativeStackNavigator();

export default function HistoryStack() {
  return (
    <Stack.Navigator initialRouteName="History">
      <Stack.Screen name="History" component={History} />
    </Stack.Navigator>
  );
}
