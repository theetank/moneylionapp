import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {enableScreens} from 'react-native-screens';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';
import TabsNavigator from './TabsNavigator';
import {colors} from '../utils/styles';
import {darkTheme} from '../utils/themes';

enableScreens();

const Stack = createNativeStackNavigator();

export default function AppNavigator() {
  /**
   * We create a stack to be able to add a token validation screen, onboarding screens
   * and other navigators. For simplicity and time constraints and not having an api
   * well just have the app section
   */
  return (
    <NavigationContainer theme={darkTheme}>
      <Stack.Navigator
        initialRouteName="App"
        screenOptions={{
          headerBackTitle: '',
          headerTintColor: colors.secondary,
          headerHideShadow: true,
          headerShown: false,
          headerStyle: {
            backgroundColor: colors.default,
          },
        }}>
        <Stack.Screen name="App" component={TabsNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
