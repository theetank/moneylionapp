import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';
import {Profile} from '../components/screens';

const Stack = createNativeStackNavigator();

export default function ProfileStack() {
  return (
    <Stack.Navigator initialRouteName="Profile">
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
}
