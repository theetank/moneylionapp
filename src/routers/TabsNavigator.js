import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import HistoryStack from './HistoryStack';
import DashboardStack from './DashboardStack';
import ProfileStack from './ProfileStack';
import {colors} from '../utils/styles';

const Tab = createMaterialBottomTabNavigator();

/**
 * Well set up seperate stack naviagtors in case we want a more
 * complex havigation in each tab
 */

export default function TabsNavigator() {
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      labeled={false}
      activeColor={colors.primary}
      barStyle={{backgroundColor: colors.secondary}}>
      <Tab.Screen
        name="History"
        component={HistoryStack}
        options={{
          tabBarIcon: ({focused, color}) => (
            <Icon
              name={focused ? 'stats-chart' : 'stats-chart-outline'}
              size={24}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Dashboard"
        component={DashboardStack}
        options={{
          tabBarIcon: ({focused, color}) => (
            <Icon
              name={focused ? 'grid' : 'grid-outline'}
              size={24}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          tabBarIcon: ({focused, color}) => (
            <Icon
              name={focused ? 'person' : 'person-outline'}
              size={24}
              color={color}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
