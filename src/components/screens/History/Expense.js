import React from 'react';
import {TouchableOpacity, StyleSheet, View, Alert} from 'react-native';
import {useDispatch} from 'react-redux';
import moment from 'moment-timezone';
import {sizes, colors} from '../../../utils/styles';
import {formatAmount} from '../../../utils';
import {Text} from '../../shared';
import {deleteExpense} from '../../../actions/expense';

const Expense = ({expense}) => {
  const dispatch = useDispatch();
  const {title, category, amount, date} = expense;
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={styles.container}
      onLongPress={() =>
        Alert.alert(
          'Are you sure you want to delete this expense?',
          null,
          [
            {
              text: 'Yes',
              onPress: () => dispatch(deleteExpense(expense)),
            },
            {text: 'Cancel', onPress: () => null},
          ],
          {
            cancelable: true,
          },
        )
      }>
      <View>
        <Text bold>{title}</Text>
        <Text style={styles.category}>{category}</Text>
        <Text small>{moment(date).format('MM/DD/YY')}</Text>
      </View>
      <Text bold size={sizes.large} color={colors.primary}>
        {formatAmount(amount)}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: sizes.small,
    paddingHorizontal: sizes.medium,
    backgroundColor: colors.secondary,
    borderBottomWidth: 1,
    borderColor: colors.gray,
    zIndex: -1,
  },
  category: {
    fontSize: sizes.small,
    marginVertical: sizes.xxsmall,
    fontStyle: 'italic',
  },
});

export default Expense;
