import React, {useState, useEffect} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
import Filter from './Filter';
import Expense from './Expense';
import {Text} from '../../shared';
import {colors, sizes} from '../../../utils/styles';

const History = () => {
  const expenses = useSelector(state => state.global.expenses);
  const [holder, setHolder] = useState(expenses);

  useEffect(() => {
    setHolder(expenses);
  }, [expenses]);
  return (
    <>
      <Filter expenses={expenses} holder={holder} setHolder={setHolder} />
      {holder.length === 0 ? (
        <View style={styles.empty}>
          <Icon name="alert-circle-outline" size={24} color={colors.primary} />
          <Text style={styles.emptyText}>
            No expenses to display, create your first expense.
          </Text>
        </View>
      ) : (
        //expenses.map((i, index) => <Expense expense={i} key={i.id} />)
        <FlatList
          data={holder}
          renderItem={({item}) => <Expense expense={item} />}
          keyExtractor={item => item.id}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: sizes.large,
  },
  emptyText: {
    marginTop: sizes.small,
  },
});

export default History;
