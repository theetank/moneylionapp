import React, {useState, useRef, useEffect} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import DatePicker from 'react-native-date-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {Button} from 'react-native-elements';
import moment from 'moment-timezone';
import {Text, Button as MyButton} from '../../shared';
import {colors, sizes, width} from '../../../utils/styles';
import {categories} from '../../../utils';

const Filter = ({expenses, holder, setHolder}) => {
  const [isVisible, setVisible] = useState(false);
  const [isVisibleE, setVisibleE] = useState(false);
  const refRBSheet = useRef();
  const refRBSheet2 = useRef();

  const [filter, setFilter] = useState({
    category: null,
    start: moment().valueOf(),
    end: moment().valueOf(),
  });

  const [dateS, setDateS] = useState(
    new Date(moment().startOf('day').valueOf()),
  );
  const [dateE, setDateE] = useState(new Date(moment().endOf('day').valueOf()));

  useEffect(() => {
    isVisible ? refRBSheet.current.open() : refRBSheet.current.close();
    isVisibleE ? refRBSheet2.current.open() : refRBSheet2.current.close();
  }, [isVisible, isVisibleE]);

  const onClear = () => {
    setFilter({
      category: null,
      start: moment().startOf('day').valueOf(),
      end: moment().endOf('day').valueOf(),
    });
    setDateS(new Date(moment().startOf('day').valueOf()));
    setDateE(new Date(moment().endOf('day').valueOf()));
    setHolder(expenses);
  };

  const onSet = () => {
    setHolder(expenses);
    if (filter.category) {
      setHolder(
        holder.filter(
          i =>
            i.category === filter.category &&
            i.date >= moment(filter.start).startOf('day').valueOf() &&
            i.date <= moment(filter.end).endOf('day').valueOf(),
        ),
      );
    } else {
      setHolder(
        holder.filter(
          i =>
            i.date >= moment(filter.start).startOf('day').valueOf() &&
            i.date <= moment(filter.end).endOf('day').valueOf(),
        ),
      );
    }
  };

  return (
    <View style={styles.container}>
      <DropDownPicker
        items={categories}
        defaultValue={null}
        placeholder="Select a Category"
        containerStyle={styles.containerStyle}
        itemStyle={styles.itemStyle}
        onChangeItem={item => setFilter({...filter, category: item.value})}
        zIndex={9000}
      />
      <View style={styles.dateContainer}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.date}
          onPress={() => setVisible(true)}>
          <Text color={'black'}>{moment(filter.start).format('MM/DD/YY')}</Text>
        </TouchableOpacity>
        <Icon name="arrow-forward" size={24} color={colors.primary} />
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.date}
          onPress={() => setVisibleE(true)}>
          <Text color={'black'}>{moment(filter.end).format('MM/DD/YY')}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttons}>
        <MyButton title="Set" primary style={styles.button} onPress={onSet} />
        <MyButton
          title="Clear"
          secondary
          onPress={onClear}
          titleStyle={{color: colors.silver}}
          style={[
            styles.button,
            {backgroundColor: 'gray', borderRadius: sizes.xsmall},
          ]}
        />
      </View>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={false}
        closeOnPressMask={false}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          container: {
            backgroundColor: colors.primary,
          },
          draggableIcon: {
            backgroundColor: colors.primary,
          },
        }}>
        <View style={styles.footer}>
          <View style={styles.footerHeader}>
            <Button
              title="Cancel"
              buttonStyle={{backgroundColor: colors.primaryLight}}
              titleStyle={{color: colors.secondary}}
              onPress={() => setVisible(false)}
            />
            <Button
              title="Done"
              buttonStyle={{backgroundColor: colors.primaryLight}}
              titleStyle={{color: colors.secondary}}
              onPress={() => {
                if (
                  moment(dateS).startOf('day').valueOf() >
                  moment(dateE).startOf('day').valueOf()
                ) {
                  setFilter({
                    ...filter,
                    start: moment(dateS).startOf('day').valueOf(),
                    end: moment(dateS).endOf('day').valueOf(),
                  });
                } else {
                  setFilter({
                    ...filter,
                    start: moment(dateS).startOf('day').valueOf(),
                  });
                }

                setVisible(false);
              }}
            />
          </View>
          <DatePicker
            date={dateS}
            onDateChange={setDateS}
            fadeToColor={colors.primary}
            textColor={colors.default}
            mode="date"
            androidVariant="iosClone"
          />
        </View>
      </RBSheet>

      <RBSheet
        ref={refRBSheet2}
        closeOnDragDown={false}
        closeOnPressMask={false}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          container: {
            backgroundColor: colors.primary,
          },
          draggableIcon: {
            backgroundColor: colors.primary,
          },
        }}>
        <View style={styles.footer}>
          <View style={styles.footerHeader}>
            <Button
              title="Cancel"
              buttonStyle={{backgroundColor: colors.primaryLight}}
              titleStyle={{color: colors.secondary}}
              onPress={() => setVisibleE(false)}
            />
            <Button
              title="Done"
              buttonStyle={{backgroundColor: colors.primaryLight}}
              titleStyle={{color: colors.secondary}}
              onPress={() => {
                if (
                  moment(dateE).endOf('day').valueOf() <
                  moment(dateS).endOf('day').valueOf()
                ) {
                  setFilter({
                    ...filter,
                    start: moment(dateE).startOf('day').valueOf(),
                    end: moment(dateE).endOf('day').valueOf(),
                  });
                } else {
                  setFilter({
                    ...filter,
                    end: moment(dateE).endOf('day').valueOf(),
                  });
                }

                setVisibleE(false);
              }}
            />
          </View>
          <DatePicker
            date={dateE}
            onDateChange={setDateE}
            fadeToColor={colors.primary}
            textColor={colors.default}
            mode="date"
            androidVariant="iosClone"
          />
        </View>
      </RBSheet>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    padding: sizes.medium,
    borderBottomWidth: 1,
    borderColor: colors.gray,
  },
  itemStyle: {justifyContent: 'flex-start'},
  containerStyle: {height: 40, marginBottom: sizes.xsmall},
  dateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  date: {
    height: 40,
    width: width / 2 - 40,
    backgroundColor: colors.default,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: sizes.xxsmall,
  },
  footer: {
    backgroundColor: colors.primaryLight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerHeader: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: sizes.xxsmall,
    paddingHorizontal: sizes.small,
  },
  buttons: {
    flexDirection: 'row',
    marginTop: sizes.small,
    //justifyContent: 'space-between',
  },
  button: {
    width: 100,
    marginRight: sizes.xsmall,
  },
});

export default Filter;
