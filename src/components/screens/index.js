import Dashboard from './Dashboard';
import History from './History';
import Profile from './Profile';

export {Dashboard, History, Profile};
