import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {useSelector} from 'react-redux';
import {Text} from '../../shared';
import AddButton from './AddButton';
import Form from './Form';
import {colors, sizes} from '../../../utils/styles';
import {formatAmount} from '../../../utils';

const Dashboard = ({navigation}) => {
  const [, setNum] = useState(0);
  const dashboard = useSelector(state => state.global.dashboard);
  const isVisible = useSelector(state => state.expense.isVisible);
  const {today, week, month, year} = dashboard;

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setNum(Math.random());
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}>
        <View style={styles.child}>
          <Text color={colors.default}>Today</Text>
          <Text style={styles.money}>{formatAmount(today)}</Text>
        </View>
        <View style={styles.child}>
          <Text color={colors.default}>Weeek</Text>
          <Text style={styles.money}>{formatAmount(week)}</Text>
        </View>
        <View style={styles.child}>
          <Text color={colors.default}>Month</Text>
          <Text style={styles.money}>{formatAmount(month)}</Text>
        </View>
        <View style={styles.child}>
          <Text color={colors.default}>Year</Text>
          <Text style={styles.money}>{formatAmount(year)}</Text>
        </View>
      </ScrollView>
      {isVisible && <Form />}
      <AddButton />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingVertical: sizes.xxsmall,
    alignItems: 'center',
  },
  child: {
    height: 250,
    width: 250,
    borderRadius: sizes.xxlarge,
    backgroundColor: colors.secondary,
    justifyContent: 'center',
    alignItems: 'center',
    margin: sizes.xxsmall,
  },
  money: {
    fontWeight: 'bold',
    fontSize: sizes.xlarge,
    marginTop: sizes.large,
    color: colors.default,
  },
});

export default Dashboard;
