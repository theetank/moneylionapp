import React, {useState, useRef, useEffect} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Overlay, Button as SmallButton} from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import DatePicker from 'react-native-date-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import DropDownPicker from 'react-native-dropdown-picker';
import moment from 'moment-timezone';
import {Text, Input, Button} from '../../shared';
import {colors, sizes, width} from '../../../utils/styles';
import {categories} from '../../../utils';
import {
  resetForm,
  addTitle,
  addAmount,
  addDate,
  addCategory,
  startUploadExpense,
} from '../../../actions/expense';

const Form = () => {
  const dispatch = useDispatch();
  const [isVisibleD, setVisibleD] = useState(false);
  const [date, setDate] = useState(new Date(moment()));
  const refRBSheet = useRef();

  const expense = useSelector(state => state.expense);

  useEffect(() => {
    isVisibleD ? refRBSheet.current.open() : refRBSheet.current.close();
  }, [isVisibleD, dispatch]);

  const onClose = () => dispatch(resetForm());

  const setDateAndClose = () => {
    dispatch(addDate(moment(date).valueOf()));
    setVisibleD(false);
  };

  return (
    <Overlay
      isVisible={expense.isVisible}
      onBackdropPress={onClose}
      overlayStyle={styles.container}>
      <View>
        <Text style={styles.title}>Create New Expense</Text>
        {expense.error && <Text style={styles.error}>{expense.error}</Text>}
        <Input
          placeholder="Title"
          rightIcon={
            <Icon name="business-outline" size={24} color={colors.lightGrey} />
          }
          onChangeText={text => dispatch(addTitle(text))}
          value={expense.title}
        />
        <Input
          placeholder="Amount"
          rightIcon={
            <Icon name="cash-outline" size={24} color={colors.lightGrey} />
          }
          keyboardType="numeric"
          onChangeText={text => dispatch(addAmount(text))}
          value={expense.amount}
        />

        <TouchableOpacity style={styles.date} onPress={() => setVisibleD(true)}>
          <Text color={'black'}>Date</Text>
          <Text color={'black'}>{moment(date).format('MM/DD/YY')}</Text>
        </TouchableOpacity>
        <DropDownPicker
          items={categories}
          defaultValue={null}
          placeholder="Select a Category"
          containerStyle={styles.containerStyle}
          itemStyle={styles.itemStyle}
          onChangeItem={item => dispatch(addCategory(item.value))}
          zIndex={4000}
        />

        <View style={styles.btnsContainer}>
          <Button
            title="Sumbit"
            onPress={() => dispatch(startUploadExpense(expense))}
            disabled={expense.loading}
            loading={expense.loading}
          />
          <Button
            title="Cancel"
            secondary
            onPress={onClose}
            disabled={expense.loading}
          />
        </View>

        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={false}
          closeOnPressMask={false}
          customStyles={{
            wrapper: {
              backgroundColor: 'transparent',
            },
            container: {
              backgroundColor: colors.primary,
            },
            draggableIcon: {
              backgroundColor: colors.primary,
            },
          }}>
          <View style={styles.footer}>
            <View style={styles.footerHeader}>
              <SmallButton
                title="Cancel"
                buttonStyle={{backgroundColor: colors.primaryLight}}
                titleStyle={{color: colors.secondary}}
                onPress={() => setVisibleD(false)}
              />
              <SmallButton
                title="Done"
                buttonStyle={{backgroundColor: colors.primaryLight}}
                titleStyle={{color: colors.secondary}}
                onPress={setDateAndClose}
              />
            </View>
            <DatePicker
              date={date}
              onDateChange={setDate}
              fadeToColor={colors.primary}
              textColor={colors.default}
              mode="datetime"
              androidVariant="iosClone"
            />
          </View>
        </RBSheet>
      </View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.gray,
    borderRadius: sizes.xsmall,
    width: width - 50,
    paddingHorizontal: sizes.large,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    paddingVertical: sizes.large,
    color: colors.default,
  },
  btnsContainer: {
    paddingVertical: sizes.large,
  },
  itemStyle: {justifyContent: 'flex-start'},
  containerStyle: {height: 40, marginVertical: sizes.xsmall},
  date: {
    height: 40,
    backgroundColor: colors.default,
    borderRadius: sizes.xxsmall,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: sizes.medium,
  },
  footer: {
    backgroundColor: colors.primaryLight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerHeader: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: sizes.xxsmall,
    paddingHorizontal: sizes.small,
  },
  error: {
    color: 'red',
    textAlign: 'center',
    marginBottom: sizes.small,
  },
});

export default Form;
