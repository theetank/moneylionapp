import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {colors, sizes} from '../../../utils/styles';
import {toggleFormVisible} from '../../../actions/expense';

const AddButton = ({setVisible}) => {
  const dispatch = useDispatch();
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.container}
      onPress={() => dispatch(toggleFormVisible())}>
      <Icon name="create-outline" color={colors.silver} size={sizes.xlarge} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    height: 50,
    width: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 15,
    right: 10,
    shadowColor: colors.secondary,
    shadowOpacity: 0.8,
    elevation: 6,
    shadowRadius: 10,
    shadowOffset: {width: 1, height: 10},
  },
});

export default AddButton;
