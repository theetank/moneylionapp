import React from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
import {Text, Avatar} from '../../shared';
import {colors, sizes} from '../../../utils/styles';

const Profile = () => {
  const user = useSelector(state => state.global.user);
  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}>
      <View style={styles.header}>
        <View style={styles.avatar}>
          <Avatar size="xlarge" title={user.name} />
          <View style={styles.edit_container}>
            <Icon name="add" size={24} color="white" />
          </View>
        </View>
        <Text center bold size={24}>
          {user.name}
        </Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    padding: sizes.large,
  },
  header: {
    marginVertical: sizes.large,
  },
  avatar: {
    alignItems: 'center',
  },
  edit_container: {
    right: -50,
    top: -40,
    width: 32,
    height: 32,
    borderRadius: 20,
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Profile;
