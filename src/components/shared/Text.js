import React from 'react';
import {Text as Element, StyleSheet} from 'react-native';
import {sizes, colors, isDark} from '../../utils/styles';

const Text = props => {
  let text = styles.normal;
  if (props.small) {
    text = styles.small;
  }
  if (props.bold) {
    text = {...text, fontWeight: 'bold'};
  }
  if (props.center) {
    text = {...text, textAlign: 'center'};
  }
  if (props.size) {
    text = {...text, fontSize: props.size};
  }
  if (props.color) {
    text = {...text, color: props.color};
  }

  return (
    <Element {...props} style={[text, props.style]}>
      {props.children}
    </Element>
  );
};

const styles = StyleSheet.create({
  normal: {
    fontSize: sizes.medium,
    color: isDark ? colors.default : colors.secondary,
  },
  small: {
    fontSize: sizes.small,
    color: isDark ? colors.default : colors.secondary,
  },
});

export default Text;
