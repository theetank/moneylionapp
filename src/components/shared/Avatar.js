import React from 'react';
import {StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Avatar as Element} from 'react-native-elements';
import {colors} from '../../utils/styles';
import {formatAvatar, config} from '../../utils';

const Avatar = props => {
  let source = null;
  if (props.source) {
    source = {uri: `${config.IMG_URL}${props.source}`};
  }
  if (props.source?.uri) {
    source = props.source;
  }
  return (
    <Element
      {...props}
      rounded
      title={formatAvatar(props.title)}
      containerStyle={styles.container}
      source={source}
      ImageComponent={FastImage}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    borderWidth: 1,
    borderColor: colors.secondary,
  },
});

export default Avatar;
