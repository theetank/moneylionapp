import React from 'react';
import {StatusBar} from 'react-native';
import {colors, isDark} from '../../utils/styles';

const Bar = ({color}) => (
  <StatusBar
    backgroundColor={isDark ? colors.secondary : colors.default}
    barStyle={isDark ? 'light-content' : 'dark-content'}
  />
);

export default Bar;
