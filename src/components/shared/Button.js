import React from 'react';
import {StyleSheet} from 'react-native';
import {Button as Element} from 'react-native-elements';
import {colors, isDark, sizes} from '../../utils/styles';

const BASE = {
  borderRadius: sizes.xsmall,
  height: sizes.xxxlarge,
};

const Button = props => {
  let style = styles.primary;
  let titleStyle = styles.primaryTitle;
  if (props.secondary) {
    style = styles.secondary;
    titleStyle = styles.secondaryTitle;
  } else if (props.mapsButton) {
    style = styles.mapsButton;
  } else if (props.timerButton) {
    style = styles.timerButton;
    titleStyle = styles.timerTitle;
  }

  return (
    <Element
      {...props}
      buttonStyle={[style, props.style]}
      titleStyle={[titleStyle, props.titleStyle]}
      disabledStyle={[style, props.style]}
      disabled={props.loading}
    />
  );
};

const styles = StyleSheet.create({
  primary: {backgroundColor: colors.primary, ...BASE},
  primaryTitle: {fontSize: 18, fontWeight: 'bold'},
  secondary: {backgroundColor: colors.gray, ...BASE},
  secondaryTitle: {fontSize: 18, color: colors.primary, fontWeight: 'bold'},
  disabled: {},
  mapsButton: {
    backgroundColor: isDark ? colors.secondary : colors.default,
  },
  timerButton: {
    backgroundColor: isDark ? colors.secondary : colors.default,
    color: colors.primary,
    // borderWidth: 2,
    // borderColor: colors.primary,
    // borderRadius: 10,
    height: 45,
  },
  timerTitle: {color: colors.primary, fontSize: 18, fontWeight: 'bold'},
});

export default Button;
