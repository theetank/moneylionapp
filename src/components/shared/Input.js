import React from 'react';
import {StyleSheet} from 'react-native';
import {Input as Element} from 'react-native-elements';
import {colors, sizes, isDark} from '../../utils/styles';

const Input = props => {
  return (
    <Element
      {...props}
      inputStyle={[styles.input, props.style]}
      inputContainerStyle={[styles.inputContainer, props.inputContainerStyle]}
      containerStyle={[styles.containerStyle, props.containerStyle]}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    fontSize: sizes.medium,
    color: colors.default,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: colors.primaryLight,
    paddingHorizontal: sizes.xsmall,
    borderRadius: sizes.xsmall,
  },
  containerStyle: {
    paddingHorizontal: 0,
    marginBottom: sizes.xxsmall,
  },
});

export default Input;
