import React from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import {colors, isDark, sizes, width} from '../../utils/styles';

const Loader = ({bridge, comments}) => {
  if (bridge) {
    return (
      <ScrollView>
        {/* bridge 1 */}
        <View style={styles.bridge}>
          <View style={styles.user} />
          <View style={styles.description} />
          <View style={styles.image} />
        </View>
        {/* bridge 2 */}
        <View style={styles.bridge}>
          <View style={styles.user} />
          <View style={styles.description} />
          <View style={styles.image} />
        </View>
        {/* bridge 3 */}
        <View style={styles.bridge}>
          <View style={styles.user} />
          <View style={styles.description} />
          <View style={styles.image} />
        </View>
      </ScrollView>
    );
  }
  if (comments) {
    return (
      <ScrollView>
        {/* post 1   */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 2 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 3 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 4 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 5 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 6 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 7 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 8 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 9 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
        {/* post 10 */}
        <View style={styles.post}>
          <View style={styles.avatar} />
          <View style={styles.postItems}>
            <View style={styles.user} />
            <View style={styles.comment} />
          </View>
        </View>
      </ScrollView>
    );
  }
  return (
    <ScrollView>
      {/* post 1   */}
      <View style={styles.post}>
        <View style={styles.avatar} />
        <View style={styles.postItems}>
          <View style={styles.user} />
          <View style={styles.description} />
          <View style={[styles.image, styles.postImage]} />
        </View>
      </View>
      {/* post 2 */}
      <View style={styles.post}>
        <View style={styles.avatar} />
        <View style={styles.postItems}>
          <View style={styles.user} />
          <View style={styles.description} />
          <View style={[styles.image, styles.postImage]} />
        </View>
      </View>
      {/* post 3 */}
      <View style={styles.post}>
        <View style={styles.avatar} />
        <View style={styles.postItems}>
          <View style={styles.user} />
          <View style={styles.description} />
          <View style={[styles.image, styles.postImage]} />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  bridge: {
    width,
    backgroundColor: isDark ? colors.secondary : colors.default,
    marginBottom: sizes.medium,
    paddingBottom: sizes.xxxlarge,
    paddingVertical: sizes.xxsmall,
  },
  post: {
    flexDirection: 'row',
    width,
    backgroundColor: isDark ? colors.secondary : colors.default,
    paddingVertical: 10,
  },
  postItems: {
    flexDirection: 'column',
    borderBottomWidth: 1,
    justifyContent: 'space-evenly',
    borderBottomColor: isDark ? colors.gray : colors.silver,
  },
  avatar: {
    width: 55,
    height: 55,
    backgroundColor: isDark ? colors.gray : colors.lightGrey,
    borderRadius: 30,
    margin: sizes.xsmall,
  },
  user: {
    backgroundColor: isDark ? colors.gray : colors.lightGrey,
    height: 15,
    width: 150,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: sizes.small,
  },
  description: {
    backgroundColor: isDark ? colors.gray : colors.lightGrey,
    height: 15,
    marginVertical: sizes.xxsmall,
    marginHorizontal: 10,
    borderRadius: sizes.small,
    width: width - 100,
  },
  comment: {
    backgroundColor: isDark ? colors.gray : colors.lightGrey,
    height: 15,
    marginVertical: sizes.xxsmall,
    marginHorizontal: 10,
    borderRadius: sizes.small,
    width: width - 100,
    marginBottom: sizes.large,
  },
  image: {
    backgroundColor: isDark ? colors.gray : colors.lightGrey,
    height: 200,
    borderRadius: sizes.small,
    marginVertical: sizes.xsmall,
    marginHorizontal: sizes.small,
    marginBottom: sizes.xxlarge,
  },
  postImage: {
    width: width - 100,
  },
});

export default Loader;
