import React from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import {colors, isDark} from '../../utils/styles';

const Loading = ({small}) => {
  if (small) {
    return (
      <View style={styles.container02}>
        <ActivityIndicator
          size="small"
          color={isDark ? colors.default : colors.secondary}
        />
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <ActivityIndicator
        size="large"
        color={isDark ? colors.default : colors.secondary}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: isDark ? colors.secondary : colors.default,
  },
  container02: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 40,
    backgroundColor: isDark ? colors.secondary : colors.default,
  },
});

export default Loading;
