import React, {useState} from 'react';
import {StyleSheet, ActivityIndicator, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import ImageView from 'react-native-image-viewing';
import {sizes, colors, isDark} from '../../utils/styles';

const Image = props => {
  const [loading, setLoading] = useState(true);
  const [isVisible, setVisible] = useState(false);

  if (isVisible) {
    return (
      <ImageView
        images={[props.source]}
        imageIndex={0}
        visible={isVisible}
        onRequestClose={() => setVisible(false)}
      />
    );
  }
  const onPress = () => (props.onPress ? props.onPress() : setVisible(true));
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <FastImage
        {...props}
        style={[styles.container, props.style]}
        onLoadEnd={() => setLoading(false)}>
        <ActivityIndicator
          animating={loading}
          color={isDark ? colors.default : colors.secondary}
        />
      </FastImage>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
    marginTop: sizes.xsmall,
    marginBottom: sizes.small,
    borderRadius: sizes.small,
    backgroundColor: isDark ? colors.gray : colors.silver,
  },
});

export default Image;
