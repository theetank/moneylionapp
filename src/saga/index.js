import {takeLatest} from 'redux-saga/effects';
import {handleUploadExpense, handleDeleteExpense} from './handlers/expense';
import {
  UPLOAD_EXPENSE_STARTED,
  REMOVE_EXPENSE_STARTED,
} from '../actionTypes/expense';

export function* watcherSaga() {
  yield takeLatest(UPLOAD_EXPENSE_STARTED, handleUploadExpense);
  yield takeLatest(REMOVE_EXPENSE_STARTED, handleDeleteExpense);
}
