import {call, put} from 'redux-saga/effects';
import {uploadExpense, deleteExpense} from '../requests/expense';
import {
  uploadExpenseSuccess,
  uploadExpenseFailed,
  deleteExpenseSuccess,
  deleteExpenseFailed,
} from '../../actions/expense';

export function* handleUploadExpense(action) {
  try {
    // use call function to handle api calls
    const payload = yield uploadExpense(action.payload);
    yield put(uploadExpenseSuccess(payload));
  } catch (e) {
    uploadExpenseFailed(e.message);
  }
}

export function* handleDeleteExpense(action) {
  try {
    // use call function to handle api calls
    const payload = yield deleteExpense(action.payload);
    yield put(deleteExpenseSuccess(payload));
  } catch (e) {
    deleteExpenseFailed(e.message);
  }
}
