// export other helpful utils etc

const formatAvatar = name =>
  name.toUpperCase().split(' ')[0].substring(0, 1) +
  (name.toUpperCase().split(' ')[1]?.substring(0, 1) ?? '');

const formatAmount = amount =>
  Number(amount).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
  });

const categories = [
  {label: 'Fixed', value: 'Fixed'},
  {label: 'Recurring', value: 'Recurring'},
  {label: 'Non-Recurring', value: 'Non-Recurring'},
  {label: 'Whammies', value: 'Whammies'},
  {label: 'Bills', value: 'Bills'},
  {label: 'Food', value: 'Food'},
  {label: 'Entertainment', value: 'Entertainment'},
];

export {formatAvatar, categories, formatAmount};
