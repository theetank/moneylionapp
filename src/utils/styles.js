import {Platform, Dimensions, Appearance} from 'react-native';

const isDark = true; //Appearance.getColorScheme() === 'dark';
const os = Platform.OS === 'ios' ? 'ios' : 'md';
const {height, width} = Dimensions.get('window');

const colors = {
  primary: '#008080',
  primaryLight: '#4ca6a6', // #329999
  secondary: '#152238',
  secondaryLight: '#2a4470',
  default: 'white',
  gray: '#1a1a1a',
  silver: '#e1e4ea', // '#86939e',
  lightGrey: '#E8E8E8',
};
const sizes = {
  xxsmall: 5,
  xsmall: 10,
  small: 12,
  medium: 16,
  large: 20,
  xlarge: 32,
  xxlarge: 40,
  xxxlarge: 50,
  topBarTitle: 20, // 22
  icon: 30,
};

export {colors, sizes, os, height, width, isDark};
