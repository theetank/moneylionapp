import {colors} from './styles';

const darkTheme = {
  dark: true,
  colors: {
    primary: colors.primary,
    background: colors.gray,
    card: colors.secondary,
    text: colors.default,
    border: colors.primary,
    notification: colors.gray,
  },
};

const lightTheme = {
  dark: false,
  colors: {
    primary: colors.secondary,
    background: colors.silver,
    card: colors.default,
    text: colors.secondary,
    border: colors.primary,
    notification: colors.gray,
  },
};

export {darkTheme, lightTheme};
