import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import moment from 'moment-timezone';
import {
  UPLOAD_EXPENSE_STARTED,
  UPLOAD_EXPENSE_SUCCESS,
  UPLOAD_EXPENSE_FAILED,
  RESET_EXPENSE_FORM,
  ADD_EXPENSE_TITLE,
  ADD_EXPENSE_AMOUNT,
  ADD_EXPENSE_DATE,
  ADD_EXPENSE_CATEGORY,
  REMOVE_EXPENSE_STARTED,
  REMOVE_EXPENSE_FAILED,
  REMOVE_EXPENSE_SUCCES,
  SET_FORM_ERROR,
  TOGGLE_VISIBLE,
} from '../actionTypes/expense';

const deleteExpense = payload => ({type: REMOVE_EXPENSE_STARTED, payload});

const deleteExpenseSuccess = payload => ({
  type: REMOVE_EXPENSE_SUCCES,
  payload,
});

const deleteExpenseFailed = payload => ({
  type: REMOVE_EXPENSE_FAILED,
  payload,
});

const startUploadExpense = payload => {
  const {title, amount, category, date} = payload;
  if (!title || title.trim() === '') {
    return {type: SET_FORM_ERROR, payload: 'Title is required'};
  }
  if (!amount || amount === '') {
    return {type: SET_FORM_ERROR, payload: 'Amount cannot be 0'};
  }
  if (!date) {
    payload.date = moment().valueOf();
  }
  if (!category) {
    return {type: SET_FORM_ERROR, payload: 'Category is required'};
  }
  payload.id = uuidv4();
  return {
    type: UPLOAD_EXPENSE_STARTED,
    payload,
  };
};

const uploadExpenseSuccess = payload => ({
  type: UPLOAD_EXPENSE_SUCCESS,
  payload,
});
const uploadExpenseFailed = payload => ({
  type: UPLOAD_EXPENSE_FAILED,
  payload,
});

const resetForm = () => ({type: RESET_EXPENSE_FORM});
const addTitle = payload => ({type: ADD_EXPENSE_TITLE, payload});
const addAmount = payload => ({type: ADD_EXPENSE_AMOUNT, payload});
const addDate = payload => ({type: ADD_EXPENSE_DATE, payload});
const addCategory = payload => ({type: ADD_EXPENSE_CATEGORY, payload});
const toggleFormVisible = () => ({type: TOGGLE_VISIBLE});

export {
  startUploadExpense,
  uploadExpenseSuccess,
  uploadExpenseFailed,
  resetForm,
  addTitle,
  addAmount,
  addDate,
  addCategory,
  toggleFormVisible,
  deleteExpense,
  deleteExpenseSuccess,
  deleteExpenseFailed,
};
